package com.example.gpsapplication.classes;

import com.example.gpsapplication.model.ModelData;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(name = "Info", database = DateBase.class)
public class TableInfo extends BaseModel {
    @PrimaryKey
    @Column
    public int item;

    @Column
    public String date;

    @Column
    public int metr;

    public int getItem() {
        return item;
    }

    public String getDate() {
        return date;
    }

    public int getMetr() {
        return metr;
    }

    public ModelData toConvert() {
        return new ModelData(date, metr, item);
    }

    public TableInfo(String date, int metr, int item) {
        this.date = date;
        this.item = item;
        this.metr = metr;
    }

    public TableInfo() {
    }
}
