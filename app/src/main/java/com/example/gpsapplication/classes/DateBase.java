package com.example.gpsapplication.classes;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = DateBase.name_base, version = DateBase.version_base)
public class DateBase {
    public static final String name_base = "InfoBase";
    public static final int version_base = 1;
}
