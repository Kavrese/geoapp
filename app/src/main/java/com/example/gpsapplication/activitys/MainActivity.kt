package com.example.gpsapplication.activitys

import android.app.Application
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.gpsapplication.R
import com.example.gpsapplication.adapter.ViewPagerAdapter
import com.example.gpsapplication.fragments.FragmentHistory
import com.example.gpsapplication.fragments.FragmentRec
import com.raizlabs.android.dbflow.config.FlowManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FlowManager.init(this)

        tab.setupWithViewPager(pager)

        pager.adapter = ViewPagerAdapter(
            supportFragmentManager,
            listOf(
                FragmentHistory(),
                FragmentRec()
            ),
            listOf("История", "Запись")
        )
        val sharedPreferences = getSharedPreferences("0", 0)

        if (sharedPreferences.getBoolean("show", true)) {

            lin_blue.setOnClickListener {
                lin_blue.visibility = View.GONE

                sharedPreferences.edit()
                    .putBoolean("show", false)
                    .apply()

            }

            lin_blue.visibility = View.VISIBLE
        }
    }
}