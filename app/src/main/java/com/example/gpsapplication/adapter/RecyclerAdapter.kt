package com.example.gpsapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.gpsapplication.R
import com.example.gpsapplication.classes.DataObject
import com.example.gpsapplication.classes.TableInfo
import com.example.gpsapplication.model.ModelData
import com.raizlabs.android.dbflow.sql.language.SQLite

class RecyclerAdapter(list: MutableList<ModelData>) :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    val list_data = list

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val metr = itemView.findViewById<TextView>(R.id.metr)
        val date = itemView.findViewById<TextView>(R.id.time)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.maket_rec, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list_data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.metr.setText("${list_data.get(position).metr} метров")
        holder.date.text = list_data.get(position).date

        holder.itemView.setOnLongClickListener(View.OnLongClickListener {
            val popurMenu = PopupMenu(holder.itemView.context, holder.itemView)
            popurMenu.inflate(R.menu.work_rec_item)
            popurMenu.setOnMenuItemClickListener {

                when (it.itemId) {
                    R.id.delete -> delete(position)

                    R.id.duplicate -> duplicate(position)
                }

                DataObject.list = list_data
                notifyDataSetChanged()

                return@setOnMenuItemClickListener true
            }
            popurMenu.show()
            return@OnLongClickListener true
        }
        )
    }

    fun delete(position: Int) {

        //Берём лист всех объектов из бд        //
        val list_bd = getAllListBD()            //  Всё
        //Удаляем всё что есть в бд             //
        deleteAllBD()                           //  Это
        //Из листа удаляем нужный нам объект    //
        list_bd.removeAt(position)              //  Костыль !
        //Сохраняем этот лист в бд              //
        saveList(list_bd)                       //

        list_data.removeAt(position)
    }

    fun duplicate(position: Int) {
        val tableInfo = TableInfo()

        tableInfo.date = list_data[position].date
        tableInfo.item = nextItem()
        tableInfo.metr = list_data[position].metr
        tableInfo.save()

        list_data.add(list_data[position])
    }

    fun getAllListBD(): MutableList<ModelData> {
        val list = SQLite
            .select().from(TableInfo::class.java)
            .queryList()
            .map { it.toConvert() }.toMutableList()
        return list
    }

    fun deleteAllBD() {
        SQLite.delete().from(TableInfo::class.java)
            .execute()
    }

    fun saveList(list: MutableList<ModelData>) {
        val tableInfo = TableInfo()
        for (l in list.iterator()) {
            tableInfo.item = l.item
            tableInfo.date = l.date
            tableInfo.metr = l.metr
            tableInfo.save()
        }
    }

    fun nextItem(): Int {
        val list = SQLite.select()
            .from(TableInfo::class.java)
            .queryList()
        return list.size + 1
    }
}