package com.example.gpsapplication.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.gpsapplication.R
import com.example.gpsapplication.adapter.RecyclerAdapter
import com.example.gpsapplication.classes.DataObject
import com.example.gpsapplication.classes.TableInfo
import com.example.gpsapplication.model.ModelData
import com.raizlabs.android.dbflow.sql.language.SQLite

class FragmentHistory : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val list = getHistoryFromBD()

        val view = inflater.inflate(R.layout.fragment_history_activity, container, false)

        val rec = view.findViewById<RecyclerView>(R.id.rec)

        rec.apply {
            layoutManager = LinearLayoutManager(rec.context)
            adapter = RecyclerAdapter(list)
            DataObject.list = list
            DataObject.rec = rec
        }
        return view
    }

    fun getHistoryFromBD(): MutableList<ModelData> {
        val list: MutableList<ModelData> = SQLite.select()
            .from(TableInfo::class.java)
            .queryList()
            .map { it.toConvert() }.toMutableList()
        return list
    }

}