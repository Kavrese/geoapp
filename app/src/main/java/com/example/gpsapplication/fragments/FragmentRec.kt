package com.example.gpsapplication.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.gpsapplication.R
import com.example.gpsapplication.classes.SimpleLis
import com.example.gpsapplication.adapter.RecyclerAdapter
import com.example.gpsapplication.classes.DataObject
import com.example.gpsapplication.classes.TableInfo
import com.example.gpsapplication.model.ModelData
import com.google.android.material.snackbar.Snackbar
import com.raizlabs.android.dbflow.sql.language.SQLite
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_rec_activity.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class FragmentRec() : Fragment() {

    val LOCAL = 1

    var oldCoordlat: Double = -200.0
    var oldCoordlon: Double = -200.0
    var latCoord: Double = -200.0
    var lonCoord: Double = -200.0
    var nowDistant: Double = 0.0

    var record = false

    private lateinit var manager: LocationManager

    private val locationCallback_for_count = object : SimpleLis() {
        override fun onLocationChanged(location: Location?) {
            val result = floatArrayOf(0f)
            latCoord = location!!.latitude
            lonCoord = location.longitude
            coord.text = "Широта: $latCoord Долгота: $lonCoord"

               if(record) {
                Location.distanceBetween(oldCoordlat, oldCoordlon, latCoord, lonCoord, result)
                //Добавляев результат к нашей старой дистанции
                nowDistant += result[0]

                text_m.text = "Пройденно ${nowDistant.toInt()} метров"
                oldCoordlat = latCoord
                oldCoordlon = lonCoord
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_rec_activity, container, false)
        val start: Button = view.findViewById(R.id.start)
        val stop: Button = view.findViewById(R.id.stop)
        val text_m = view.findViewById<TextView>(R.id.text_m)

        manager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        start.setOnClickListener {
            if (checkConnect(manager)) {
                //Если координаты были когда-то определены - запуск записи
                if (latCoord != -200.0 && lonCoord != -200.0) {
                    record = true

                    start.isEnabled = false
                    stop.isEnabled = true

                    oldCoordlat = latCoord
                    oldCoordlon = lonCoord

                    Snackbar.make(stop, "Старт записи", Snackbar.LENGTH_SHORT).show()
                } else {
                    //Если нет, то выводим сообщение
                    Snackbar.make(
                        start,
                        "Подождите, идёт вычисление координат",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            } else {
                checkSettings()
            }
        }
        stop.setOnClickListener {
            start.isEnabled = true
            stop.isEnabled = false

            text_m.text = "Пройденно 0 метров"

            if (nowDistant == 0.0) {
                Snackbar.make(stop, "Сохранять ? Вы нечего не прошли", Snackbar.LENGTH_LONG)
                    .setAction("Да") {
                        Snackbar.make(stop, "Сохраненно", Snackbar.LENGTH_SHORT).show()
                        saveToHistory(nowDistant)
                    }.setActionTextColor(resources.getColor(R.color.colorPrimary))
                    .show()
            }else{
                Snackbar.make(stop, "Сохраненно", Snackbar.LENGTH_SHORT).show()
                saveToHistory(nowDistant)
            }
            nowDistant = 0.0
        }
        return view
    }

    fun saveToHistory(nowDistant: Double) {
        val list = DataObject.list
        val modelData = ModelData(
            SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(
                Date()
            ), nowDistant.toInt(), nextItem()
        )
        list.add(modelData)

        DataObject.list = list
        DataObject.rec.adapter = RecyclerAdapter(list)

        saveToBD(modelData)
    }

    fun saveToBD(modelData: ModelData) {
        val tableInfo = TableInfo(modelData.date, modelData.metr, modelData.item)
        tableInfo.save()
    }

    fun nextItem(): Int {
        val list = SQLite.select()
            .from(TableInfo::class.java)
            .queryList()
        return list.size + 1
    }

    override fun onStart() {
        super.onStart()

        //Проверка настроек
        checkSettings()

        if (!canUseGEO()) {
            //Запрос разрешений
            usedGEO()
        } else {
            //Поиск локации
            getLocal()
        }
    }

    override fun onStop() {
        super.onStop()
        if (::manager.isInitialized) {
            manager.removeUpdates(locationCallback_for_count)
        }
    }

    private fun canUseGEO(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            android.Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun usedGEO() {
        requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCAL)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCAL) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Всё ок
                requireActivity().lin_red.visibility = View.GONE
                //Проверка настроек
                checkSettings()
                getLocal()
            } else {
                requireActivity().lin_red.visibility = View.VISIBLE

                requireActivity().lin_red.setOnClickListener {
                    usedGEO()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun getLocal() {
        val criteria = Criteria()

        criteria.accuracy = Criteria.ACCURACY_FINE
        criteria.powerRequirement = Criteria.POWER_HIGH

        manager.requestLocationUpdates(
            TimeUnit.SECONDS.toMillis(1),
            1f,
            criteria,
            locationCallback_for_count,
            Looper.getMainLooper()
        )
    }

    fun checkSettings() {
        val result = checkConnect(manager)
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        if (!result) {
            requireActivity().lin_red_2.visibility = View.VISIBLE
            requireActivity().lin_red_2.setOnClickListener {
                if (!checkConnect(manager))
                    startActivity(intent)
                else
                    requireActivity().lin_red_2.visibility = View.GONE
            }
        } else {
            requireActivity().lin_red_2.visibility = View.GONE
        }
    }

    fun checkConnect(manager: LocationManager): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            manager.isLocationEnabled
        } else {
            Settings.Secure.getInt(
                requireContext().contentResolver,
                Settings.Secure.LOCATION_MODE,
                Settings.Secure.LOCATION_MODE_OFF
            ) != Settings.Secure.LOCATION_MODE_OFF
        }
    }
}